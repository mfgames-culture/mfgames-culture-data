MfGames Culture Data
====================

Welcome the [MfGames Culture](https://mgames.com/mfgames-culture/) data files repository/package. This documentation describes the JSON-formatted files that consist of the package and used to represent real-world cultural elements.

Repository Layout
-----------------

The layout of the repository is:

* `./data/`: The source directory which contains the JSON and YAML files which describe the various components of a culture.
* `./dist/`: A Git-ignored file which contains versions of the data files (in the same directory structure), plus:
	* `./dist/combined.json`: A sequence containing every data file as a single file.
	* `./dist/combined.min.json`: A minified version of `./dist/combined.json`.
	* `./dist/index.json`: A sequence of every data file but only with the `version`, `id`, `type`, `description`, and `keywords` elements.
	* `./dist/index.min.json`: A minified version of `./dist/index.json`.
* `./docs/`: Documentation written in Markdown and using full filenames for links.
* `./lib/`: The single JavaScript source so this package can be `require`d.

Files
-----

Data files can either be JSON or YAML data. The file name is typically lowercase, but may include capitals for IEFT language tags.

* `./data/en-US.json`
* `./data/gregorian.json`

The `id` attribute in the file must match its directory structure, minus the `./data/` and the `.json` extension. For example, `en-US` or `gregorian`.

* [Cultures](cultures.md)
* [Calendars](calendars.md)

JSON data files can have single-line comments (//) which are stripped out before they are processed for the `dist` folder.

Other
-----

* [Versions](versions.md)
* [Glossary](glossary.md)
