Versioning
==========

This repository uses [semver](https://semver.org/). The version is in `package.json`.

Major Versions
--------------

* A source or data file is renamed or removed.
* Organization of the data files has changed.

Minor Version
-------------

* A data file has additional elements add it to it.

Patch
-----

* Typographical or documentation changes were made to data files.
